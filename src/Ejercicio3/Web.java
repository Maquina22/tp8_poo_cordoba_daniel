
package Ejercicio3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import javax.swing.*;
/*
 * @ DANIEL CORDOBA
 */
public class Web extends JFrame{
    public JPanel panel;
    private JTextField texto;
    public Web(){
        setSize(450, 250);  //tamaño de la ventana        
        setLocationRelativeTo(null);   //ventana centrada 
        iniciarComponentes();       
        setDefaultCloseOperation(EXIT_ON_CLOSE); //fin de la ejecucion 
    }
    
    private void iniciarComponentes(){
        colocarPanel();
        etiqueta();
        etiqueta1();
        cajaTexto();
        boton();
    }
    
    private void colocarPanel(){
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.BLACK);
        this.getContentPane().add(panel);
    }
    
    private void etiqueta(){        
        JLabel etiquetaURL = new JLabel("Ingresar URL");
        etiquetaURL.setBounds(30, 60, 100, 40);
        etiquetaURL.setForeground(Color.WHITE);
        etiquetaURL.setFont(new Font("Times New Roman", 0, 16));        
        panel.add(etiquetaURL);
    }
    
    private void etiqueta1(){        
        JLabel etiquetaURL = new JLabel("Descargar Contenido Pagina Web");
        etiquetaURL.setBounds(30, 10, 600, 40);
        etiquetaURL.setForeground(Color.WHITE);
        etiquetaURL.setFont(new Font("Cooper Black", 0, 20));        
        panel.add(etiquetaURL);
    }
    
    private void cajaTexto(){
        texto = new JTextField();
        texto.setBounds(150, 70, 200, 20);
        panel.add(texto);
    }
    
    private void boton(){
        JButton guardar = new JButton("Guardar");
        guardar.setBounds(170, 150, 100, 40);
        guardar.setForeground(Color.WHITE);
        guardar.setFont(new Font("Cooper Black", 0, 15));
        guardar.setBackground(Color.GREEN);
        panel.add(guardar);
        ActionListener accion = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                descargarWeb();
            }
        };        
        guardar.addActionListener(accion);        
    }

    public void descargarWeb(){
        String nombre = null;
        try {
            nombre = JOptionPane.showInputDialog (" Ingresar nombre y extension del archivo ");
            URL url=new URL(texto.getText());            
            BufferedReader bf= new BufferedReader(new InputStreamReader(url.openStream()));
            File archivo= new File(nombre);            
            BufferedWriter bw= new BufferedWriter(new FileWriter(archivo));
            String cad;
            while((cad=bf.readLine())!=null){
                bw.write(cad);
            }            
            bf.close();
            bw.close();
            JOptionPane.showMessageDialog(null, "Archivo guardado!");                    
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(null, "Mal la URL ");                    
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Sitio web no encontrado");                    
        }        
    }
}
