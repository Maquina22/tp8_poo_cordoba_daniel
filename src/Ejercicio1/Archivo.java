package Ejercicio1;

import java.io.File; 
import java.io.FileNotFoundException; 
import java.util.*;

/*
 * @author DANIEL CORDOBA
 */
public class Archivo {

    public static void main(String[] args) {
        int rta = 0;
        String nombre = null;
        Scanner leer = new Scanner(System.in);         
        do {
            
            try {
                System.out.print("\nNombre del archivo: ");
                nombre = leer.nextLine();
                Scanner input = new Scanner(new File("C:\\Users\\DCORDOBA\\Documents\\NetBeansProjects\\TP6_POO_CORDOBA_DANIEL\\"+nombre+".txt"));
                while (input.hasNextLine()){
                    String line = input.nextLine();
                    System.out.println(line);
                }
                input.close();                
            } catch (FileNotFoundException ex) {
                System.out.println("\nArchivo " + nombre + " no encontrado!");
            }
            try{
                System.out.print("\n1. Si desea leer otro archivo o cualquier otra tecla para Salir: ");
                rta = leer.nextInt();               
            }catch(InputMismatchException ex){
                rta = 0;
            }
            leer.nextLine();
        }while (rta == 1);
    }
}

